const userJobs = require('./userJobs');

const fetchWebDevelopers = (userJobs) => {
    const webDevelopers = userJobs.filter((user) => {
        return user.job.toLowerCase().includes('web developer');
    });
    return webDevelopers;
}

const salaryInNumbers = (userJobs) => {
    const users = userJobs.map((user) => {
        user.salary = Number(user.salary.replace('$', ''));
        return user;
    });
    return users;
}

const correctSalary = (userJobs) => {
    const FACTOR = 10000;

    const users = userJobs.map((user) => {
        const correctSalary = FACTOR * Number(user.salary.replace('$', ''));
        return { ...user, corrected_salary: correctSalary };
    });
    return users;
}

const sumSalaries = (userJobs) => {
    const FACTOR = 10000;
    const ans = userJobs.reduce((totalSalary, user) => {
        let correctSalary = FACTOR * Number(user.salary.replace('$', ''));
        return totalSalary += correctSalary;
    }, 0);
    return ans;
}

const sumSalariesByCountry = (userJobs) => {
    let salarySumByCountry = {};
    const FACTOR = 10000;

    const countries = userJobs.reduce((countries, user) => {
        return countries.concat(user.location);
    }, []).filter((country, index, countries) => {
        return countries.indexOf(country) === index;
    }).sort();

    countries.forEach((country) => {

        let totalSalary = userJobs.filter((user) => {
            return user.location === country;
        }).reduce((totalSalary, user) => {
            let correctSalary = FACTOR * Number(user.salary.replace('$', ''));
            return totalSalary += correctSalary;
        }, 0);

        salarySumByCountry[country] = totalSalary;
    });

    return salarySumByCountry;
}

const calculateAverageByCountry = (userJobs) => {
    let salaryAverageByCountry = {};
    const FACTOR = 10000;

    const countries = userJobs.reduce((countries, user) => {
        return countries.concat(user.location);
    }, []).filter((country, index, countries) => {
        return countries.indexOf(country) === index;
    }).sort();

    countries.forEach((country) => {
        let usersInCountry = 0;

        let totalSalary = userJobs.filter((user) => {
            usersInCountry++;
            return user.location === country;
        }).reduce((totalSalary, user) => {
            let correctSalary = FACTOR * Number(user.salary.replace('$', ''));
            return totalSalary += correctSalary;
        }, 0);

        salaryAverageByCountry[country] = Number((totalSalary / usersInCountry).toFixed(2));
    });

    return salaryAverageByCountry;
}


